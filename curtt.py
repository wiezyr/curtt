import curses
import sqlite3
from sqlite3 import Error
import time
from datetime import datetime
import os

DB_DIR = f"{os.path.expanduser('~')}/.config/curtt"
DATABASE = f"{DB_DIR}/database.db"

SQL_CREATE_TASK_TABLE = """ CREATE TABLE IF NOT EXISTS tasks (
        id integer PRIMARY KEY,
        name text NOT NULL UNIQUE,
        duration integer
    ); """

SQL_CREATE_TIME_TABLE = """CREATE TABLE IF NOT EXISTS timer(
        id integer PRIMARY KEY,
        name text NOT NULL,
        start_time text NOT NULL,
        end_time text NOT NULL,
        duration integer NOT NULL
    );"""

INFO = [
    "Command             - Action",
    "-" * 44,
    "start {task number} - start task timer",
    "add {task name}     - add new task",
    "del {task number}   - delete task",
    "exit                - to end the pogram",
]

if not os.path.exists(DB_DIR):
    os.makedirs(DB_DIR)


def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)

    except Error as e:
        print(e)

    return conn


conn = create_connection(DATABASE)


def create_table(create_table_sql):
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)


def main_win(screen):
    screen.border()
    curses.curs_set(1)
    curses.start_color()
    curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE)
    BLACK_AND_WHITE = curses.color_pair(1)
    num_rows, num_cols = screen.getmaxyx()
    if num_rows < 26 or num_cols < 106:
        curses.endwin()
        print("Terminal must have at least 26 rows and 106 columns for app to work")
        exit()
    middle_column = int(num_cols / 2)
    screen.addstr(
        0, middle_column - 10, " Curtt - time tracker ", BLACK_AND_WHITE | curses.A_BOLD
    )

    screen.refresh()

    task_win = screen.subwin(20, 50, 5, 55)
    timer_win = screen.subwin(5, 50, 5, 3)
    info_win = screen.subwin(15, 50, 10, 3)
    cmd_win = screen.subwin(3, 102, 2, 3)

    return task_win, timer_win, info_win, cmd_win


def task_window(task_win):
    task_win.clear()
    task_win.box()

    cur = conn.cursor()
    cur.execute(""" SELECT id, name FROM tasks ORDER BY id ASC """)
    tasks = cur.fetchall()
    task_win.addstr(0, 3, f" Existing tasks: {len(tasks)} ")
    for i, task in enumerate(tasks):
        task_win.addstr(i + 1, 2, f"{task[0]}. {task[1]}")

    task_win.refresh()


def info_window(info_win, content):
    info_win.clear()
    info_win.box()
    info_win.addstr(0, 3, " Info ")
    for i, text in enumerate(content):
        info_win.addstr(i + 2, 2, text)
    info_win.refresh()


def timer_window(timer_win, selected_task=" No task selected "):
    timer_win.clear()
    timer_win.box()
    timer_win.addnstr(0, 3, selected_task, 30)
    timer_win.refresh()


def command_window(cmd_win):
    cmd_win.clear()
    cmd_win.box()
    cmd_win.addstr(0, 2, "Write your command here: ")
    cmd_win.addstr(1, 2, "> ")
    # cmd_win.move(1, 5)
    curses.echo()
    cmd = cmd_win.getstr().decode("utf-8")
    cmd_win.refresh()
    return cmd


def add_task(conn, task_name):
    sql = """ INSERT INTO tasks (name, duration) VALUES (?,?) """
    cur = conn.cursor()
    cur.execute(sql, (task_name, 0))
    conn.commit()
    cur.close()


def delete_task(id):
    cur = conn.cursor()
    cur.execute("SELECT name FROM tasks WHERE id=?", (id,))
    task = cur.fetchone()
    #   try:
    task_name = task[0]
    # except TypeError:

    cur.execute("DELETE FROM tasks WHERE id=?", (id,))
    # conn.commit()
    cur.execute("DELETE FROM timer WHERE name=?", (task_name,))
    conn.commit()
    cur.close()


def task_choice(task_id):
    cur = conn.cursor()
    cur.execute("SELECT id, name FROM tasks WHERE id=?", (task_id,))
    task = cur.fetchone()

    # print(f"Your chosen task is: {task}")
    chosen_task = task[1]
    return chosen_task


def timer(screen, timer_win):
    start = datetime.now()
    while True:
        screen.nodelay(1)
        duration = (datetime.now() - start).seconds
        hour = duration // 3600
        seconds = duration % 3600
        minutes = seconds // 60
        seconds %= 60

        timer_win.addstr(
            2, 2, f"Counting..        - {hour:02d}:{minutes:02d}:{seconds:02d} "
        )

        timer_win.refresh()
        time.sleep(1)
        if screen.getch() >= 0:
            timer_win.addstr(2, 2, " Timer stopped ")
            timer_win.refresh()
            screen.nodelay(False)
            break
    stop = datetime.now()
    start_time = start.strftime("%Y:%m:%d %H:%M:%S")
    stop_time = stop.strftime("%Y:%m:%d %H:%M:%S")
    duration = (stop - start).seconds
    # write_time(task_in_use, start_time, stop_time, duration, conn)
    return start_time, stop_time, duration


def write_time(name, start, stop, duration):
    cur = conn.cursor()
    cur.execute(
        "INSERT INTO timer (name,start_time,end_time,duration) VALUES (?,?,?,?)",
        (name, start, stop, duration),
    )
    conn.commit()
    old_dur = cur.execute(""" SELECT duration FROM tasks WHERE name=?""", (name,))
    cur.execute(
        """ UPDATE tasks SET duration=? WHERE name=? """,
        (duration + old_dur.fetchone()[0], name),
    )
    conn.commit()


def main(screen):
    # conn = create_connection(DATABASE)

    # create tables
    if conn is not None:
        # create tasks table
        create_table(SQL_CREATE_TASK_TABLE)

        # create timer table
        create_table(SQL_CREATE_TIME_TABLE)
    else:
        print("Error! cannot create the database connection.")

    curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
    RED_AND_BLACK = curses.color_pair(2)
    curses.init_pair(3, curses.COLOR_GREEN, curses.COLOR_BLACK)
    GREEN_AND_BLACK = curses.color_pair(3)

    task_win, timer_win, info_win, cmd_win = main_win(screen)
    task_window(task_win)
    timer_window(timer_win)
    info_window(info_win, INFO)
    while True:
        cmd = command_window(cmd_win)

        if cmd.startswith("add "):
            add_task(conn, cmd[4:])
            task_window(task_win)
        elif cmd.startswith("del "):
            timer_window(timer_win, f" Task selected for deletion: {cmd[4:]} ")
            try:
                delete_task(cmd[4:])
                timer_win.addstr(
                    2, 2, f"Task number {cmd[4:]} was deleted", GREEN_AND_BLACK
                )
                task_window(task_win)
            except TypeError:
                timer_win.addstr(
                    2, 2, f"Task number {cmd[4:]} does not exists", RED_AND_BLACK
                )
            timer_win.refresh()
        elif cmd.startswith("start "):
            try:
                selected_task = task_choice(cmd[6:])
            except TypeError:
                timer_win.addstr(
                    2, 2, f"Task number {cmd[6:]} does not exists", RED_AND_BLACK
                )
                timer_win.refresh()
                continue
            timer_window(timer_win, f" Active task: {selected_task} ")
            info_window(info_win, ["press any key to stop timer"])
            start_time, stop_time, duration = timer(screen, timer_win)
            write_time(selected_task, start_time, stop_time, duration)
            info_window(info_win, INFO)
            timer_win.addnstr(0, 3, f" '{selected_task}' - task completed ", 30)
            timer_win.refresh()
        elif cmd == "exit":
            break
        else:
            timer_window(timer_win)
            timer_win.addstr(2, 2, "Wrong command, try again.")
            timer_win.refresh()


if __name__ == "__main__":
    curses.wrapper(main)
